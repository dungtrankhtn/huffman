﻿#pragma once

class CMABIT
{
public:
	char bits[N_ASCII]; // chứa dãy bít 0/7
	int n_bit;

	CMABIT()
	{
		n_bit = 0;
	}

	void addBit(char bit)
	{
		bits[n_bit] = bit;
		n_bit++;
		bits[n_bit] = '\0';
	}

	void deleteBit()
	{
		n_bit--;
		bits[n_bit] = '\0';
	}

	/*void output()
	{
		for (int i = 0; i < n_bit; i++)
			cout << bits[i];
		cout << endl;
	}*/
};