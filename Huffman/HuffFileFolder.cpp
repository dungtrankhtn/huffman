﻿//#include "CHuffFileFolder.h"
//
//CHuffFileFolder::CHuffFileFolder()
//{
//	n_file = 0;
//}
//
//unsigned long CHuffFileFolder::sizeFile(string file_adr)
//{
//	ifstream f_in(file_adr);
//
//	f_in.seekg(0, ios::end);
//
//	return f_in.tellg();
//}
//
//void CHuffFileFolder::setFileInAdr(char file_adr_in[MAX_ADDRESS])
//{
//	strcpy(this->file_in_adr, file_adr_in);
//}
//
//unsigned long CHuffFileFolder::convertStringToNum(string s)
//{
//	unsigned long x = 0;
//	int length = s.length();
//	for (int i = 0; i < length; i++)
//	{
//		x = x * 10 + s[i] - 48;
//	}
//
//	return x;
//}
//
//void CHuffFileFolder::getFileNameAndSizeFile(string s, string &file_name, unsigned long &size)
//{
//	string temp;
//	int length = s.length();
//	int i = length - 1;
//
//	while (s[i] != ' ')
//	{
//		i--;
//	}
//	file_name = s.substr(0, i);
//
//	string str_num = s.substr(i + 1, s.length() - i - 2);
//	size = convertStringToNum(str_num);
//}
//
//void CHuffFileFolder::readFileFolder()
//{
//	ifstream file_in(file_in_adr, ios::binary);
//
//	file_in >> n_file;
//	list_file_name.resize(n_file);
//	list_size_file.resize(n_file);
//
//	// lấy tên file và size ban đầu
//	string temp_str;
//	getline(file_in, temp_str);
//	for (int i = 0; i < n_file; i++)
//	{
//		getline(file_in, temp_str);
//
//		getFileNameAndSizeFile(temp_str, list_file_name[i], list_size_file[i]);
//	}
//
//	list_location_file.resize(n_file); // danh sách vị trí của từng file
//	list_sizeCompress_file.resize(n_file);
//
//	// đọc size sau nén của từng file và vị trí read của từng file
//	for (int i = 0; i < n_file; i++)
//	{
//		file_in >> list_sizeCompress_file[i];
//		list_location_file[i] = file_in.tellg();
//		list_location_file[i]++;
//		//cout << list_location_file[i] << endl;
//		//cout << list_sizeCompress_file[i] << endl;
//		file_in.seekg(list_sizeCompress_file[i], ios::cur);
//	}
//}
//
//void CHuffFileFolder::createFolder()
//{
//	int length = strlen(file_in_adr);
//	int i = length - 1;
//
//	while (file_in_adr[i] != '.')
//	{
//		i--;
//	}
//
//	for (int j = 0; j < i; j++)
//	{
//		folder_adr[j] = file_in_adr[j];
//	}
//	folder_adr[i] = '\0';
//
//	mkdir(folder_adr);
//}
//
//string CHuffFileFolder::converCharToString(char *str)
//{
//	string s;
//	s.resize(strlen(str) + 1);
//
//	for (int i = 0; i < strlen(str); i++)
//		s[i] = str[i];
//	s[strlen(str)] = '\0';
//
//	s.erase(s.length() - 1, 1);
//
//	return s;
//}
//
//void CHuffFileFolder::creatAdrFileOut()
//{
//	string folder_adr_string = converCharToString(folder_adr);
//
//	list_adr_file_out.resize(n_file);
//
//	for (int i = 0; i < n_file; i++)
//	{
//		list_adr_file_out[i] = folder_adr_string + "\\" + list_file_name[i];
//		//cout << list_adr_file_out[i];
//	}
//}
//
//void CHuffFileFolder::view()
//{
//	readFileFolder(); // đọc thông tin file nén
//
//	cout << "|------------------------------------------------------------------------|" << endl;
//
//	cout << "|" << setw(4) << "STT" << "|" << setw(SPACE_NAME) << "Ten File" << "|" << setw(SPACE_NUM) << "Size truoc nen" << "|" << setw(SPACE_NUM) << "Size sau nen" << "|" << endl;
//
//	cout << "|------------------------------------------------------------------------|" << endl;
//
//	for (int i = 0; i < n_file; i++)
//	{
//		cout << "|" << setw(4) << i + 1 << "|" << setw(SPACE_NAME) << list_file_name[i] << "|" << setw(SPACE_NUM) << list_size_file[i] << "|" << setw(SPACE_NUM) << list_sizeCompress_file[i] << "|" << endl;
//
//		cout << "|------------------------------------------------------------------------|" << endl;
//	}
//}
//
//int CHuffFileFolder::decompressionPartFolder(vector<int> list_part)
//{
//	if (n_file == 0)
//		readFileFolder(); // đọc thông tin file nén
//
//	createFolder(); // tạo thư mục chứa file giải nén
//	creatAdrFileOut(); // tạo danh sách địa chỉ các file giải nén
//
//	ifstream f_in(file_in_adr, ios::binary);
//
//	int n = list_part.size();
//
//	for (int i = 0; i < n; i++)
//	{
//		int i_file = list_part[i] - 1;
//
//		CHuffFile x;
//		ofstream f_temp("temp.huff", ios::binary);
//		f_in.seekg(list_location_file[i_file], ios::beg);
//
//		unsigned char ch_temp;
//		int count = 0;
//		while (1)
//		{
//			f_in >> noskipws >> ch_temp;
//			count++;
//			if (count == list_sizeCompress_file[i_file])
//				break;
//			f_temp << ch_temp;
//		}
//		f_temp.close();
//
//		x.decompressionFile("temp.huff", list_adr_file_out[i_file]);
//	}
//
//	remove("temp.huff"); // xóa file tạm
//
//	return 1;
//}
//
//int CHuffFileFolder::decompressionAllFolder()
//{
//	if (n_file == 0)
//		readFileFolder(); // đọc thông tin file nén
//
//	createFolder(); // tạo thư mục chứa file giải nén
//	creatAdrFileOut(); // tạo danh sách địa chỉ các file giải nén
//
//	ifstream f_in(file_in_adr, ios::binary);
//
//	for (int i = 0; i < n_file; i++)
//	{
//		CHuffFile x;
//		ofstream f_temp("temp.huff", ios::binary);
//		f_in.seekg(list_location_file[i], ios::beg);
//
//		unsigned char ch_temp;
//		int count = 0;
//		while (1)
//		{
//			f_in >> noskipws >> ch_temp;
//			count++;
//			if (count == list_sizeCompress_file[i])
//				break;
//			f_temp << ch_temp;
//		}
//		f_temp.close();
//
//		x.decompressionFile("temp.huff", list_adr_file_out[i]);
//
//		if (sizeFile(list_adr_file_out[i]) != list_size_file[i])
//			cout << "Tap tin " << list_file_name[i] << " bi loi" << endl;
//	}
//
//	remove("temp.huff"); // xóa file tạm
//
//	return 1; // hoàn thành việc giải nén
//}