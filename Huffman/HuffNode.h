#pragma once

#ifndef _HUFFNODE_
#define _HUFFNODE_

#include <iostream>
using namespace std;

class CHuffNode
{
public:
	unsigned char c;
	int freq;
	bool used;

	int left; 
	int right;

	friend bool operator>(CHuffNode x,CHuffNode y);
	friend bool operator>=(CHuffNode x, CHuffNode y);
	friend bool operator<(CHuffNode x, CHuffNode y);
	friend bool operator<=(CHuffNode x, CHuffNode y);
	friend bool operator==(CHuffNode x, CHuffNode y);

	void output()
	{
		cout << "c=" << c << "/freq=" << freq << "/left=" << left << "/right=" << right << endl;
	}
};

bool operator>(CHuffNode x, CHuffNode y)
{
	return x.freq > y.freq;
}

bool operator>=(CHuffNode x, CHuffNode y)
{
	return x.freq >= y.freq;
}

bool operator<(CHuffNode x, CHuffNode y)
{
	return x.freq < y.freq;
}

bool operator<=(CHuffNode x, CHuffNode y)
{
	return x.freq <= y.freq;
}

bool operator==(CHuffNode x, CHuffNode y)
{
	return (x.freq == y.freq);
}

#endif