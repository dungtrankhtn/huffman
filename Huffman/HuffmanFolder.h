﻿#pragma once

#include "HuffmanFile.h"
#include "Folder.h"

class CHuffmanFolder
{
public:
	CFolder folder;
	char folder_in_adr[MAX_ADDRESS];
	char file_out_adr[MAX_ADDRESS];
	//---------------------------------//

	void setFolderInAdr(char folder_in_adr[MAX_ADDRESS])
	{
		strcpy(this->folder_in_adr, folder_in_adr);
		strcpy(folder.folder_adr, folder_in_adr);
	}

	void setFileOutAdr(char file_out_adr[MAX_ADDRESS])
	{
		strcpy(this->file_out_adr, file_out_adr);
	}

	// ghi thông tin các file nén vào đầu file
	void writeInforListFile()
	{
		ofstream file_out(file_out_adr);

		file_out << folder.n_file << endl;

		for (int i = 0; i < folder.n_file; i++)
		{
			file_out << folder.list_file_name[i] << " " << folder.list_size_file[i] << endl;
		}
		file_out.close();
	}

	// ghép file src vào cuối file dest
	void addFileToFile(string file_dest, string file_src)
	{
		ifstream f_src(file_src, ios::binary);
		ofstream f_dest(file_dest, ios::app | ios::binary);

		unsigned long size_src;
		f_src.seekg(0, ios::end);
		size_src = f_src.tellg();
		f_src.seekg(0, ios::beg);

		f_dest << size_src << endl;
		unsigned char ch;
		while (1)
		{
			f_src >> noskipws >> ch;
			if (f_src.eof())
				break;
			f_dest << ch;
		}
	}

	// nén thư mục
	int compressionFolder()
	{
		folder.readFolder();
		writeInforListFile();

		for (int i = 0; i < folder.n_file; i++)
		{
			CHuffFile huff_file;
			huff_file.compressionFile(folder.list_file_adr[i], "temp.huff");
			addFileToFile(file_out_adr, "temp.huff");
		}

		remove("temp.huff");

		return 1;
	}
};