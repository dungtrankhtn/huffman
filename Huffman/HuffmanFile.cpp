﻿//#include "HuffmanFile.h"
//
////---------------------//
//// Nen file
////---------------------//
//
//// tìm dãy bit của ký tự ch
//int CHuffFile::findBit(unsigned char ch, int index)
//{
//	if (tree.huff_tree[index].left == -1 && tree.huff_tree[index].right == -1)
//	{
//		if (tree.huff_tree[index].c == ch)
//			return 1;
//		else
//			return 0;
//	}
//
//	ma_bit[ch].addBit('0');
//	if (findBit(ch, tree.huff_tree[index].left) == 1)
//		return 1;
//	else
//	{
//		ma_bit[ch].deleteBit();
//		ma_bit[ch].addBit('1');
//		if (findBit(ch, tree.huff_tree[index].right) == 1)
//			return 1;
//
//		ma_bit[ch].deleteBit();
//	}
//}
//
//// tạo dãy bit của từng ký tự
//void CHuffFile::makeBit()
//{
//	for (int i = 0; i < N_ASCII; i++)
//	{
//		if (tree.huff_tree[i].freq>0)
//		{
//			ma_bit[i].n_bit = 0;
//			findBit(i, tree.size - 1);
//			//cout << i << "," << tree.huff_tree[i].freq << ": ";
//			//ma_bit[i].output();
//		}
//	}
//}
//
//// lưu lại bảng tần suất vào file
//void CHuffFile::saveFreq(string file_name_out)
//{
//	ofstream f_out(file_name_out, ios::binary);
//
//	f_out << tree.countDiferChar() << endl; // ghi số ký tự khác nhau có trong file input
//	//cout << f_out.tellp() << " ";
//	for (int i = 0; i < N_ASCII; i++)
//	{
//		if (tree.huff_tree[i].freq>0)
//		{
//			f_out << i << " " << tree.huff_tree[i].freq << endl; // ghi mã ký tự và tần số
//			//cout << f_out.tellp() << " ";
//		}
//	}
//	//cout << f_out.tellp() << endl;
//}
//
//int CHuffFile::countNumber(unsigned long x)
//{
//	int count = 0;
//	while (1)
//	{
//		x = x / 10;
//		count++;
//		if (x == 0)
//			break;
//	}
//
//	return count;
//}
//
//unsigned long CHuffFile::countByteOfFile(string adr_file_in)
//{
//	unsigned long count_byte = 0;
//	count_byte += 4 * 2 + 4 + 1;
//	//cout << count_byte << endl;
//
//	for (int i = 0; i < N_ASCII; i++)
//	{
//		if (tree.huff_tree[i].freq>0)
//		{
//			//count_byte += 1+1+
//			//cout << countNumber(i) << " " << countNumber(tree.huff_tree[i].freq) << endl;
//			cout << count_byte << endl;
//		}
//	}
//	cout << count_byte << endl;
//
//	ifstream f_in(adr_file_in, ios::binary);
//	unsigned char char_in;
//
//	int count_bit = 0;
//	while (1)
//	{
//		f_in >> noskipws >> char_in;
//		if (f_in.eof())
//			break;
//
//		count_bit += ma_bit[char_in].n_bit;
//		if (count_bit >= 8)
//		{
//			count_byte += (count_bit / 8);
//			count_bit = count_bit % 8;
//		}
//	}
//	if (count_bit > 0)
//		count_byte++;
//
//	count_byte++;
//
//	return count_byte;
//}
//
//// nén file
//void CHuffFile::compressionFile(string file_adr_in, string file_adr_out)
//{
//	tree.readFromFile(file_adr_in);
//	//tree.outputTreeNot();
//	tree.buildTree();
//	//tree.outputTree();
//
//	makeBit();
//
//	saveFreq(file_adr_out);
//
//	ifstream f_in(file_adr_in, ios::binary);
//	ofstream f_out(file_adr_out, ios::binary | ios::app);
//
//	unsigned char ch;
//	unsigned char out = 0;
//	int n_out = 7;
//
//	while (1)
//	{
//		f_in >> noskipws >> ch;
//		if (f_in.eof())
//			break;
//
//		for (int i = 0; i < ma_bit[ch].n_bit; i++)
//		{
//			if (ma_bit[ch].bits[i] == '1')
//				out = out | (1 << (n_out));
//
//			n_out--;
//
//			if (n_out == -1)
//			{
//				f_out << out;
//				n_out = 7;
//				out = 0;
//			}
//		}
//	}
//	if (out > 0)
//		f_out << out;
//
//	f_out << 7 - n_out;
//
//	f_out << endl;
//
//	//location_write = f_out.tellp();
//
//	f_out.close();
//	f_in.close();
//
//}
//
////---------------------//
//// giai nen file
////---------------------//
//
//// đọc bảng tuần số của file decompress
//int CHuffFile::readFile(string file_name)
//{
//	ifstream f_in(file_name, ios::binary);
//
//	int n_difer_char;
//	f_in >> n_difer_char;
//
//	int ch_int;
//	for (int i = 0; i < n_difer_char; i++)
//	{
//		f_in >> ch_int;
//		tree.huff_tree[ch_int].c = ch_int;
//		f_in >> tree.huff_tree[ch_int].freq;
//	}
//	//f_in >> n_last_bit;
//
//	return f_in.tellg();
//}
//
//int CHuffFile::reviewBits(unsigned char &ch, CMABIT bit)
//{
//	int index_huffnode = tree.size - 1;
//	for (int i = 0; i < bit.n_bit; i++)
//	{
//		if (bit.bits[i] == '1')
//			index_huffnode = tree.huff_tree[index_huffnode].right;
//		else
//			index_huffnode = tree.huff_tree[index_huffnode].left;
//	}
//
//	if (tree.huff_tree[index_huffnode].left == -1 && tree.huff_tree[index_huffnode].right == -1)
//	{
//		ch = tree.huff_tree[index_huffnode].c;
//		return 1; // đã tìm thấy nút nhánh
//	}
//	else
//		return 0; // chưa tìm thấy
//}
//
//void CHuffFile::decompressionFile(string file_adr_in, string file_adr_out)
//{
//	int location_read = readFile(file_adr_in);
//	//tree.outputTreeNot();
//	tree.buildTree();
//	//tree.outputTree();
//
//	ifstream f_in(file_adr_in, ios::binary);
//	f_in.seekg(location_read + 1);
//
//	ofstream f_out(file_adr_out, ios::binary);
//
//	unsigned char ch_get;
//	unsigned char in;
//	unsigned char in_2;
//	unsigned char in_3;
//
//	int n_in = 7;
//	CMABIT bit;
//	int bit_get1;
//	bit.n_bit = 0;
//
//	while (1)
//	{
//		f_in >> noskipws >> in;
//		location_read = f_in.tellg();
//
//		f_in >> noskipws >> in_2;
//		f_in >> noskipws >> in_3;
//		if (f_in.eof())
//			break;
//		f_in.seekg(location_read);
//
//		while (1)
//		{
//			bit_get1 = (in >> n_in) & 1;
//			if (bit_get1 == 1)
//				bit.addBit('1');
//			else
//				bit.addBit('0');
//			n_in--;
//
//			if (reviewBits(ch_get, bit) == 1)
//			{
//				f_out << ch_get;
//				bit.n_bit = 0;
//			}
//
//			if (n_in == -1)
//			{
//				n_in = 7;
//				break;
//			}
//		}
//	}
//
//	// xử lý byte cuối cùng (đặc biệt)
//	n_last_bit = in_2 - 48;
//	if (n_last_bit == 0)
//		n_last_bit = 8;
//	for (int i = 0; i < n_last_bit; i++)
//	{
//		bit_get1 = (in >> n_in) & 1;
//		if (bit_get1 == 1)
//			bit.addBit('1');
//		else
//			bit.addBit('0');
//		n_in--;
//
//		if (reviewBits(ch_get, bit) == 1)
//		{
//			f_out << ch_get;
//			bit.n_bit = 0;
//		}
//	}
//}