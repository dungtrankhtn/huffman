﻿#pragma once

#include "HuffNode.h"
#include <fstream>
#include <string>

#define MAX_NODES 600
#define N_ASCII 256

using namespace std;

class CHuffTree
{
public:
	CHuffNode huff_tree[MAX_NODES];
	int size; // tổng số phần tử (dùng để chạy vòng lặp tìm kiếm)
	int n_used; // những số không thuộc bảng thống kế
	int n_not_used; // những số thuộc bảng thống kê

	CHuffTree()
	{
		size = N_ASCII;
		n_used = n_not_used = 0;
		for (int i = 0; i < N_ASCII; i++)
		{
			huff_tree[i].c = i;
			huff_tree[i].freq = 0;
			huff_tree[i].used = 0;
			huff_tree[i].left = -1;
			huff_tree[i].right = -1;
		}
	}

	void outputTree()
	{
		for (int i = 0; i < size; i++)
		{
			if (huff_tree[i].freq>0 && huff_tree[i].used == 1)
			{
				cout << i << ": ";
				huff_tree[i].output();
			}
		}
		cout << endl;
	}

	void outputTreeNot()
	{
		for (int i = 0; i < size; i++)
		{
			if (huff_tree[i].freq>0 && huff_tree[i].used == 0)
			{
				cout << i << ": ";
				huff_tree[i].output();
			}
		}
		cout << endl;
	}

	// lập bảng tần số ký tự trong file
	int readFromFile(string file_name)
	{
		ifstream f_in(file_name, ios::binary);
		unsigned char ch;
		while (1)
		{
			f_in >> noskipws >> ch;
			if (f_in.eof())
				break;
			huff_tree[ch].freq++;
		}

		f_in.close();

		return 1;
	}

	// đếm số ký tự khác nhau xuất hiện
	int countDiferChar()
	{
		int count = 0;
		for (int i = 0; i < N_ASCII; i++)
		{
			if (huff_tree[i].freq>0)
				count++;
		}

		return count;
	}

	// tạo cây Huffman
	void buildTree()
	{
		n_not_used = countDiferChar();
		n_used = 0;

		int i_min1, i_min2;
		while (n_not_used > 1)
		{
			find2MinNode(i_min1, i_min2);

			huff_tree[size].c = '0';
			huff_tree[size].freq = huff_tree[i_min1].freq + huff_tree[i_min2].freq;
			huff_tree[size].left = minNode(i_min1, i_min2);
			if (huff_tree[size].left == i_min1)
				huff_tree[size].right = i_min2;
			else
				huff_tree[size].right = i_min1;

			huff_tree[size].used = 0;
			n_not_used += 1;

			size++;

			huff_tree[i_min1].used = 1;
			huff_tree[i_min2].used = 1;
			n_not_used -= 2;
		}
		huff_tree[size - 1].used = 1;
	}

	// tìm 2 node nhỏ nhất trong bảng
	void find2MinNode(int &id_min_1, int &id_min_2)
	{
		id_min_1 = -1;
		id_min_2 = -1;
		for (int i = 0; i < size; i++)
		{
			if (((huff_tree[i] < huff_tree[id_min_1]) || id_min_1 == -1) && huff_tree[i].used == 0 && huff_tree[i].freq>0)
			{
				id_min_1 = i;
			}
		}
		huff_tree[id_min_1].used = 1;

		for (int i = 0; i < size; i++)
		{
			if (((huff_tree[i] < huff_tree[id_min_2]) || id_min_2 == -1) && huff_tree[i].used == 0 && huff_tree[i].freq>0)
			{
				id_min_2 = i;
			}
		}

		huff_tree[id_min_1].used = 0;
	}

	int minNode(int i_a, int i_b)
	{
		if (huff_tree[i_a] < huff_tree[i_b])
			return i_a;

		if (huff_tree[i_a] == huff_tree[i_b])
		{
			if (huff_tree[i_a].c < huff_tree[i_b].c)
				return i_a;
			return i_b;
		}

		return i_b;
	}

	int maxNode(int i_a, int i_b)
	{
		if (huff_tree[i_a] > huff_tree[i_b])
			return i_a;

		if (huff_tree[i_a] == huff_tree[i_b])
		{
			if (huff_tree[i_a].c > huff_tree[i_b].c)
				return i_a;
			return i_b;
		}
		return i_b;
	}
};